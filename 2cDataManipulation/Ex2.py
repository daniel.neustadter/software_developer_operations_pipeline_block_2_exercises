#!/usr/bin/env python3

given_string = input('\nType a given string. Separate parts with a comma. Default: \"Smith,Maynard,E1,USAF\". ')
if given_string == '':
    given_string="Smith,Maynard,E1,USAF"
    originals = True
else:
    originals = False
given_string = list(given_string.split(","))
print('\nYour String Converted to a list: ' + str(given_string))
is_in = input('\nType what to check for in the string. Default: \"Snuffy\". ')
if is_in == '':
    is_in='Snuffy'
    print('\nChecking for: ' + is_in)
if is_in in given_string:
    print('\nChecking for: ' + is_in)
    print ('\n' + str(is_in) + ' is in ' + str(given_string) + '\n')
else:
    print('\n' + is_in + ' is not in given string.\n')

print('Printing full list: \n')
for items in given_string:
    print(items)

if (originals == True):
    print('\nDefault list in \"Rank FirstName, LastName\" format: \n')
    #Last name = Smith, First name = Maynard, Rank = E1, Branch = USAF
    print(given_string[2] +' ' + given_string[1] + ' , ' + given_string[0] + '\n')


# For a given string, "Smith,Maynard,E1,USAF":
# - Determine if the string 'Snuffy' is in the string and print whether it is or not.
# - Split the string into a list and print each item in the list.
# - Print the string in the format "Rank FirstName, LastName" using elements of the list