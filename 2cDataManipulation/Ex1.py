#!/usr/bin/env python3
original_string = input('Type in your string: ')

print('\nDisplay the input in console: ' + original_string)
print('Display the input in title text: ' + original_string.title())
print('Swap the case of the input and display it: ' + original_string.swapcase())
print('Remove all spaces in the input and display it: ' + original_string.replace(" ", ""))
print('Display the input as uppercase: ' + original_string.upper())
print('Display the input as lowercase: ' + original_string.lower())
print('Capitalize the input and display it: ' + original_string.capitalize())


# Take input from the user and display the original string along with manipulated forms of the string.
# - Take input from the user.
# - Display the input in console.
# - Display the input as title text.
# - Swap the case of the input and display it.
# - Remove all spaces in the input and display it.
# - Display the input as uppercase.
# - Display the input as lowercase.
# - Capitalize the input and display it.