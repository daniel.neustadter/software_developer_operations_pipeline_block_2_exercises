#!/usr/bin/env python3

print('How many hours did you work?')
hours = int(input())

per_hour = 10

if hours > 40 and hours <= 50:
    per_hour = per_hour*1.5
if hours > 50:
    per_hour = per_hour*2

print(hours*per_hour)

