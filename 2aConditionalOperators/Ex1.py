#!/usr/bin/env python3

print('Good morning, what is your name?')
name = str(input())
if name == 'Criminal':
    print('Denied entry')

else:
    print('Welcome to Keesler AFB, ' + name + '!')
