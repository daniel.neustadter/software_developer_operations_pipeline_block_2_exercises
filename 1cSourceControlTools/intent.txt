This project is established to ensure proper USAF dormitory accountability.
Members who contribute to this project are expected to make sure that:
- Only USAF members who are allowed to be in the dorms will be checked into the dorms
- USAF members who are restricted from entering the dorms will be denied entry.
- There will not be duplicate members entering the dorms (i.e. if there is an Amn Snuffy in the dorms, another Amn Snuffy cannot be added in - They must be Amn Snuffy.1 or similar)
- All dorm members should be checked in by the time accountability is collected - 2300, or they are considered late.
- Each Dorm member should be checked in according to their phase:
- Phase 1 : 2100
- Phase 2 : 2200
- Phase 3 : 2200
- Red Card / Restricted : 2000
