#!/usr/bin/env python3
username = str(input('Login: '))
password = str
count = int(0)
while password != 'password':
    password = str(input('Password: '))
    count = count + 1
    if password == 'password':
        print('Welcome ' + username)
    elif count==1:
        print('Wrong password! You have tried and failed ' + str(count) + ' time!')
    if password == ' password':
        print('Welcome ' + username)
    elif count > 1:
        print('Wrong password! You have tried and failed ' + str(count) + ' times!')
    if count == 3:
        exit()

#Create a python program that will utilize a while loop that accepts a user password.
#- If correct it will print out welcome {user name} and if wrong printout 'wrong password! You have tried and failed X# of Times!'
#- Then keep waiting till user types right password.
#- The while loop will count how many attempts have been made
#- If there have been 3 failed attempts then let the user know and quit the application.
#- Password = 'password'
