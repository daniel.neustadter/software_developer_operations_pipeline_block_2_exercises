#!/usr/bin/env python3
first_number=float(input('\nEnter first number: '))
second_number=float(input('Enter second number: '))
operator=str(input('\nChoose an operator. (add, subtract, multiply, divide) \nFor subtraction and division, use first_number-second_number or first_number/second_number\n\n'))
def addition():
    print(first_number+second_number)
def subtraction():
    print(first_number-second_number)
def multiplication():
    print(first_number*second_number)
def division():
    print(first_number/second_number)
if operator=='add' or operator=='addition':
    addition()
elif operator=='subtract' or operator=='subtraction':
    subtraction()

elif operator=='divide' or operator=='division':
        if second_number==0:
            print('\nundefined')
        else:
            division()
elif operator=='multiply' or 'multiplication':
    multiplication()
else:
    exit()
print()

# Create a number of functions to add, subtract, multiply, and divide.
# - Take input from the user for two numbers and the operator (add, subtract, multiply, divide)
# - Given the inputs, call the function that returns the result of the operation
# - Print the result of the function