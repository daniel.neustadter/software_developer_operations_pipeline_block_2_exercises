#!/usr/bin/env python3
import random, sys
how_many = int(sys.argv[1])
list = []
for j in range(how_many):
    list.append(random.randint(1, 10))
print(list)

#Create a program that will add a number of random numbers to a list
#- This program will take input from the user specifying how many random numbers to generate
#- Use the random library to generate random integers between 1 and 10
#- Input taken from the user will utilize the imported sys library's sys.argv list of arguments
#- Print to console every random number from that list
