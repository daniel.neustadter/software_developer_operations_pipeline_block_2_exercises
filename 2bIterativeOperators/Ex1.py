#!/usr/bin/env python3
for x in range (0,101):
    if x%5==0 and x%3==0:
        print('FizzBuzz')
    elif x%3==0:
        print('Fizz')
    elif x%5==0:
        print('Buzz')
    else:
        print(str(x))


#Create a for loop that will count to 100.
#- It will print out the number unless:
#-- The number is divisible by 3, then it prints out "Fizz"
#-- The number is divisible by 5, then it prints out "Buzz"
#-- The number is divisible by 3 and 5, then it prints out "FizzBuzz"
